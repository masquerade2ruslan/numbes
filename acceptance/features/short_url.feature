Feature: Url shortening
  
  Scenario: A guest can try shortening service
  	Given I have a long url like "http://thisisthelongesturliveseeninmylive.com/add/here"
    When I click Shrink it
    Then I should get a "http://numb.es/."