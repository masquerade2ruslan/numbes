When(/^I open "(.*?)"$/) do |url|
  @response = RestClient.get url
end

Then(/^I should see application logo$/) do
  @response.code.should == 200
end

