Given(/^I have a long url like "(.*?)"$/) do |lnk|
  @url = lnk
end

When(/^I click Shrink it$/) do
  @page = RestClient.post "http://localhost/shrink", {:url => @url}
end

Then(/^I should get a "(.*?)"$/) do |arg1|
  @page.should =~ /numb.es/
  puts @page
end
