%% Copyright
-module(memo_event_handler).
-author("rsln").

-behaviour(gen_event).

%% gen_event
-export([init/1, handle_event/2, handle_call/2, handle_info/2, terminate/2,
  code_change/3]).

%% gen_event callbacks
-record(state, {}).

init(_Args) ->
  {ok, #state{}}.

handle_event({url_shortened, Short, Original}, State) ->
  ok = memo:remember(Short, Original),
  io:format("***url_shortened*** ~p~p~n", [Short, Original]),
  {ok, State};
handle_event({original_resolved, Short}, State) ->
  ok = memo:track(Short),
  io:format("***original_resolved*** ~p~n", [Short]),
  {ok, State};
handle_event(_Event, State) ->
  {ok, State}.

handle_call(_Request, State) ->
  {ok, reply, State}.

handle_info(_Info, State) ->
  {ok, State}.

terminate(_Arg, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
