-module(memo).
-behaviour(application).
-include("memo_records.hrl").

-export([install/1]).
-export([start/2, stop/1]).
-export([remember/2, fetch/1, track/1, stats/0, urls/0, last_remembered/0]).
 
start(normal, []) ->
	mnesia:wait_for_tables([urls, stats], 5000),
	memo_sup:start_link().
 
stop(_) -> ok.

install(Nodes) ->
	ok = mnesia:create_schema(Nodes),
	application:start(mnesia),
	mnesia:create_table(urls,
		[{attributes, record_info(fields, urls)},{disc_copies, Nodes}, {type, ordered_set}]),
	mnesia:create_table(stats,
		[{attributes, record_info(fields, stats)},{disc_copies, Nodes}]),
  application:stop(mnesia)

.


remember(Short, Original) ->
  F = fun() ->
		mnesia:write(#urls{token=Short, original=Original}),
		mnesia:write(#stats{token=Short, original=Original, times=0})
	end,
	mnesia:activity(transaction, F).

last_remembered()->
  case mnesia:activity(transaction, fun()-> mnesia:last(urls) end) of
    '$end_of_table' -> "a";
    Last -> Last
  end.



fetch(Short) ->
	F = fun() -> mnesia:read({urls, Short}) end,
	case mnesia:activity(transaction, F) of
		[] -> undefined;
		[#urls{original=Original}] -> {ok,Original}
	end.

track(Short) ->
  Increment = fun() ->
		[Persisted] = mnesia:read({stats, Short}),
		Count = Persisted#stats.times + 1,
		Updated = Persisted#stats{times = Count},
		mnesia:write(Updated)
	end,
	{atomic, ok} = mnesia:transaction(Increment),
	ok.

stats() ->
        mnesia:transaction(fun()-> qlc:e(mnesia:table(stats)) end).

urls() ->
        mnesia:transaction(fun()-> qlc:e(mnesia:table(urls)) end).
