-module(memo_SUITE).

-include_lib("common_test/include/ct.hrl").
-include("/home/rsln/dev/numbes/apps/memo/src/memo_records.hrl").

-export([init_per_suite/1, end_per_suite/1, all/0, fetch/1]).

-export([remember/1, track/1]).

all() -> [remember, track, fetch].
 
init_per_suite(Config) ->
	Priv = ?config(priv_dir, Config),
	application:set_env(mnesia, dir, Priv),
	memo:install([node()]),
	application:start(mnesia),
	application:start(memo),
	Config.
 
end_per_suite(_Config) ->
	application:stop(mnesia),
	ok.

remember(_) ->
	ok = memo:remember("g00gl3", "http://www.google.com").

track(_) ->
	F = fun() ->  
		mnesia:write(#stats{token="google", original="w.ggl.com", times=1}) end,
	mnesia:activity(transaction, F),
	ok = memo:track("google"),
	{_, _, 2} = get_stat("google").

fetch(_) ->
	F = fun() ->
                mnesia:write(#urls{token="google", original="w.ggl.com"}) end,
        mnesia:activity(transaction, F),
	{ok, "w.ggl.com"} = memo:fetch("google").

get_stat(Token) ->
	F = fun() ->
		case mnesia:read({stats, Token}) of
		  [#stats{token=Tkn, original=O, times=T}] -> {Tkn, O, T};
		  [] -> undefined
		end
	end,
	mnesia:activity(transaction, F).
