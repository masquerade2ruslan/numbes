-module(sequencer).

-behaviour(gen_server).

-export([start_link/0]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-export([next/0, bootstrap/1]).

-record(state, {count = 0}).

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

bootstrap(Seq) ->
    gen_server:call(?MODULE, {bootstrap, Seq}).

next() ->
    gen_server:call(?MODULE, {next}).

init([]) ->
    {ok, #state{count = 0}}.

handle_call({bootstrap, Seq}, _From, State) ->
  NewState = State#state{count = Seq},
  {reply, {ok, NewState#state.count}, NewState};

handle_call({next}, _From, State) ->
    Count = State#state.count,	
    NewState = State#state{count = Count + 1},
    {reply, {ok, NewState#state.count}, NewState}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

