-module(urlshrinker).

-export([shorten/1, continue_after/1]).

continue_after(Last)->
  Last_id = bijector:decode(Last),
  sequencer:bootstrap(Last_id).

shorten(_Original) ->
	{ok, Seq} = sequencer:next(),
	{ok, bijector:encode(Seq)}.
