-module(bijector).
-export([encode/1,decode/1]).

-define(Abc, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").

decode(String)->decode(String,0).

decode([Char | Rest],Prod) ->
    NewProd = Prod * 64 + string:str(?Abc,[Char]) -1,
    decode(Rest,NewProd);
decode([],Prod) -> Prod.


encode(Number,Reminders) ->
    case Number > 0 of
        true ->
            Reminder = Number rem 62,
            encode(floor(Number/62),[Reminder|Reminders]);
        false ->
            lists:map(fun(X) -> lists:nth(X+1,?Abc) end, Reminders)
   end.
encode(Number) -> encode(Number,[]).


floor(X) when X < 0 ->
    T = trunc(X),
    case X - T == 0 of
        true -> T;
        false -> T - 1
    end;

floor(X) -> 
    trunc(X).


