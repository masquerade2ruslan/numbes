-module(bijector_tests).
-include_lib("eunit/include/eunit.hrl").

encode_for_125_returns_cb_test() ->
    ?assertEqual("cb", bijector:encode(125)).

encode_for_2__returns_ac_test() ->
    ?assertEqual("c", bijector:encode(2)).


decode_for_empty_string_returns_0_test() ->
    ?assertEqual(0, bijector:decode("")).

decode_for_ac_returns_2_test() ->
    ?assertEqual(2, bijector:decode("ac")).
