%% Copyright
-module(event_bus).
-author("rsln").
-compile(export_all).

%% API
-export([]).

init()->
  gen_event:start({local, ?MODULE}),
  gen_event:add_handler(?MODULE, memo_event_handler, []).

url_shortened(Short, Original)->
  gen_event:notify(?MODULE, {url_shortened, Short, Original}).

original_resolved(Short)->
  gen_event:notify(?MODULE, {original_resolved, Short}).