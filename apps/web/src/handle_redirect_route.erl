-module(handle_redirect_route).

-export([init/3]).
-export([handle/2]).
-export([terminate/3]).

init(_Transport, Req, []) ->
	{ok, Req, undefined}.

handle(Req, State) ->
	{Token, Req1} = cowboy_req:binding(token, Req),
  {ok, OriginalUrl} = app_service:get_original(Token),
	{ok, Req2} =  cowboy_req:reply(302,[{<<"Location">>, OriginalUrl}], "", Req1),
	{ok, Req2, State}.

terminate(_Reason, _Req, _State) ->
	ok.
