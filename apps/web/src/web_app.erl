%% Feel free to use, reuse and abuse the code in this file.

%% @private
-module(web_app).
-behaviour(application).

%% API.
-export([start/2]).
-export([stop/1]).

%% API.

start(_Type, _Args) ->
	Dispatch = cowboy_router:compile([
		{'_', [
			{"/", cowboy_static,
				[{directory, {priv_dir, web, []}},
				{mimetypes, [{<<".html">>, [<<"text/html">>]}]}, 
				{file, <<"index.html">>}]},

			{"/js/[...]", cowboy_static,
     				[{directory, {priv_dir, web, [<<"js">>]}},
      				{mimetypes, {fun mimetypes:path_to_mimes/2, default}}]},		
			{"/css/[...]", cowboy_static,
                                [{directory, {priv_dir, web, [<<"css">>]}},
                                {mimetypes, {fun mimetypes:path_to_mimes/2, default}}]},

			{"/img/[...]", cowboy_static,
                                [{directory, {priv_dir, web, [<<"img">>]}},
                                {mimetypes, {fun mimetypes:path_to_mimes/2, default}}]},

			{"/shrink", handle_shrink_route, []},
			{"/:token", handle_redirect_route, []}
		]}
	]),
	{ok, _} = cowboy:start_http(http, 100, [{port, 80}], [
		{env, [{dispatch, Dispatch}]}
	]),

  app_service:init(),
  event_bus:init(),
	web_sup:start_link().

stop(_State) ->
	ok.
