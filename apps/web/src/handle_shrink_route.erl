-module(handle_shrink_route).

-export([init/3]).
-export([handle/2]).
-export([terminate/3]).

init(_Transport, Req, []) ->
	{ok, Req, undefined}.

handle(Req, State) ->
	{Method, Req2} = cowboy_req:method(Req),
	HasBody = cowboy_req:has_body(Req2),
	{ok, Req3} = maybe_url(Method, HasBody, Req2),
	{ok, Req3, State}.

maybe_url(<<"POST">>, true, Req) ->
	{ok, PostBody, Req2} = cowboy_req:body_qs(Req),
	Url = proplists:get_value(<<"url">>, PostBody),
  ShortUrl = app_service:shrink(Url),
  cowboy_req:reply(200, [{<<"content-encoding">>, <<"utf-8">>}], ShortUrl, Req2);
maybe_url(_, _, Req) ->
	cowboy_req:reply(405, Req).


terminate(_Reason, _Req, _State) ->
	ok.
