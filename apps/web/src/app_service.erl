-module(app_service).

-export([shrink/1, get_original/1, init/0]).

shrink(Url) ->
  {ok, NextUrl} = urlshrinker:shorten(Url),
  event_bus:url_shortened(NextUrl, Url),
  string:concat("http://numb.es/",NextUrl).

get_original(Url) ->
  ShortUrl = binary_to_list(Url),
  event_bus:original_resolved(ShortUrl),
  memo:fetch(ShortUrl).

init()->
  Last = memo:last_remembered(),
  urlshrinker:continue_after(Last).

